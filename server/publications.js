
Meteor.publish('posts', function() {
  return Posts.find({owner: this.userId}, { sort: { createdAt: -1 } });
});

Meteor.publish('articles', function() {
  return articles.find({owner: this.userId}, { sort: { createdAt: -1 } });
});

Meteor.publish("currentAccessToken", function(){
  return Meteor.users.find(this.userId, {fields: {'services.facebook.accessToken': 1}});
});

import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import twit from 'twit';
import fb from 'fb';

Meteor.startup(() => {
  // code to run on server at startup
});

//var Twit = Meteor.npmRequire('twit');
var FeedParser = Npm.require('feedparser');
var request = Npm.require('request'); // for fetching the feed 

var Twit = require('twit');
var T = new Twit({
  consumer_key:         'NyLYBgWPzWb1bEMO3dePuHjtL',
  consumer_secret:      '3g5dY3LAgng4jcOsBt9tVbKMQjPMHJKCNP0E0NBRHtqxaGQeso',
  access_token:         '836998943619821569-A777wfSIepa0ZwoUkGyLRqGZXptiF7R', 
  access_token_secret:  'lK1aEJTjjCYZBben6ZXRMaORa5rXLY2Wm4tLauJkODsrP',
  timeout_ms:           60*1000
});
var face = require('fb');
var F = new face.Facebook({
  appId:        '1837160072976278',
  appSecret:    '0239917993cb5af36addb6f356a5ac7e',
  scope:        'publish_actions'
});
const cities = require("all-the-cities");

var addRssFeedsToCollection = function(rssItems) {
  rssItems.forEach(function(entry) {
    
    articles.insert({
      title : entry.title,
      pubDate : new Date(entry.date),
      description : entry.description,
      link : entry.link,
    });

  });
}

Meteor.methods({

  'getFeed': function(url) {

    console.log("in getFeed");

    var req = request(url);
    var feedparser = new FeedParser();

    var callGetFeed = Meteor.wrapAsync(feedparser.on, feedparser);

    req.on('error', function (error) {
      // handle any request errors 
      console.log("req error");
    });
     
    req.on('response', function (res) {
      console.log("req response");
      var stream = this; // `this` is `req`, which is a stream 
      if (res.statusCode !== 200) {
        this.emit('error', new Error('Bad status code'));
      }
      else {
        stream.pipe(feedparser);
      }
    });
     
    feedparser.on('error', function (error) {
      // always handle errors 
      console.log("feedparser error " + error);
    });

    callGetFeed('readable',
      function () {
        var stream = feedparser; 
        var item;
        rssFeedItems = [];
        while (item = stream.read()) {
          rssFeedItems.push(item);
        }
        addRssFeedsToCollection(rssFeedItems);
    });

  },

  twitterSearch: function(keyword){
    var lat = null;
    var lon = null;
    const getTweets = Meteor.wrapAsync(T.get,T);
    this.tweets = new ReactiveVar();
    tweets = getTweets('search/tweets', { q: keyword, count: 10 }).statuses;
    var userId = this.userId;
    tweets.forEach(function(row){
      check(row.text, String);
    //  console.log(row);
      // console.log(row.coordinates + "   " + row.place);
      if (row.coordinates != null){
        lat = row.coordinates.coordinates[0];
        lon = row.coordinates.coordinates[0];
      }else if(row.place != null){
        city = Meteor.call('getCity', row.place.name, row.place.country_code);
        console.log(city);
        lat = city[0].lat;
        lon = city[0].lon;
      }

    	Posts.insert({
    		text: row.text,
    		createdAt: new Date(),
      	owner: userId,
      	status: 'Fav',
        lat: lat,
        lon: lon,
        source: 'Twitter'
    	});
    });
  }, 

  getCity: function(cityName, countryName){
    return cities.filter(function(city){
      return city.name.match(cityName) && city.country.match(countryName);
    });
  },

  postTwitter: function(tweet){
    console.log("post tweet " + tweet);
    SyncedCron.add({
      name: 'Post tweet',
      schedule: function(parser) {
        return parser.text('every 1 min');
      },
      job: function() {
        T.post('statuses/update', {status: tweet}, function(err, data, response) {
          console.log(data)
        });
      }
    });
    SyncedCron.start();
  },

  statusChange: function(ele_id, tweet_status){
    check(ele_id, String);
    check(tweet_status, String);
    Posts.update(ele_id, {$set: {status: tweet_status}});
  },

  postFacebook: function(status, access_token){
    console.log(access_token);
    F.setAccessToken(access_token);
    F.api('me/feed', 'post', {scope: 'publish_actions', message: status}, function(res){
      if(!res || res.error) {
        console.log(!res ? 'error occurred' : res.error);
      }
      console.log('Post Id: ' + res.id);
    });
  }
});

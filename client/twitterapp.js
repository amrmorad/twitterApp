import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './twitterapp.html';

var lat = null;
var lon = null;

Template.tweetsOps.onCreated(function() {
	this.subscribe('posts');
	this.subscribe('currentAccessToken');
});

Template.showFeeds.onCreated(function() {
    this.subscribe('articles');
});

Template.form.events = {
    'click button#getFeed' : function(evt) {
        evt.preventDefault();
        var feed = document.getElementById('feed').value;
        Meteor.call('getFeed', feed);
    },
}

Template.showFeeds.helpers({
    entries() {
        return articles.find({}, { sort: { createdAt: -1 }, count: 10 });
    },
});

Template.mapModal.onCreated(function() {
    console.log("ceated");
    GoogleMaps.load({key: 'AIzaSyDb3UNAmfGuILlRjZlJqZY3cDGMd0G9e8k'});
});

Template.mapModal.onRendered(function() {
    GoogleMaps.load({key: 'AIzaSyDb3UNAmfGuILlRjZlJqZY3cDGMd0G9e8k'});
    console.log("lllllllllllll");
    //await sleep(2000);
    GoogleMaps.ready('gMap', function(map) {
        console.log('before  lat '+lat+" "+lon);
        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(lat, lon),
          map: map.instance
        });
    });
});

Template.tweetsOps.helpers({
	tweets: function(){
		return Posts.find({}, { sort: { createdAt: -1 }, count: 10 });
	},

	tweetsstatus: function(status){
		statuses = ['Fav', 'Mark', 'Done', 'Reply', 'Read later'];
		const index = statuses.indexOf(status);
        statuses.splice(index, 1);
        return statuses;
    },
});

Template.mapModal.helpers({
  defineGoogleMap: function() {
    if (GoogleMaps.loaded()) {
      console.log('loaded');
      return {
        center: new google.maps.LatLng(lat, lon),
        zoom: 8
      };
    }
  }
});

Template.tweetsOps.events = {
    'click button#searchTwitter' : function (event, instance) {
    	var keyword = document.getElementById('keyword');
    	if (keyword.value != '') {
  			Meteor.call('twitterSearch', keyword.value);
        	document.getElementById('keyword').value = '';
    	}
    },

    'click button#postTwitter' : function (event) {
    	var tweet = document.getElementById('tweetPost');
    	if (tweet.value != '') {
  			Meteor.call('postTwitter', tweet.value);
        	document.getElementById('tweetPost').value = '';
    	}
    },

    'click button#showMap' : function (event) {
        post = Posts.findOne({_id: this._id});
        console.log(post);
        lat = post.lat;
        lon = post.lon;
        console.log("post "+lat+" "+lon);
        //post.lat and post.lon
        Modal.show('mapModal');
    },


    'click button#postFacebook' : function (event) {
  		var accessToken = Meteor.user().services.facebook.accessToken;
    	console.log(accessToken);
    	var post = document.getElementById('tweetPost');
    	if (post.value != '') {
  			Meteor.call('postFacebook', post.value, accessToken);
        	document.getElementById('tweetPost').value = '';
    	}
    },

    'click button#updateTweetStatus': function (event) {
    	var e = document.getElementById('tweetstatus');
        var status = e.options[e.selectedIndex].text;
        Meteor.call('statusChange', this._id, status);
    }
};